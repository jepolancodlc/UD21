package views;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.*;
import java.awt.*;

public class VentanaCalc extends JFrame {

	 
	private static final long serialVersionUID = 1L;
	static double resFact = 1, num1;
	double num2;
	String op;
	private JPanel contentPane;
	private static JTextArea Historial;
	private static JTextField pantallita,pantalla;
	private JButton btn_exp,btn_log,btn_pi,btn_2nd,btn_e,btn_c,btn_Delete,btn_x2,btn_1x,btn_xBarras,btn_mod,btn_2vx,
	btn_parentesisAbrir,btn_parentesisCerrar,btn_n,btn_dividir,btn_xy,btn,btn_8,btn_9,btn_multiplicar,btn_10x,btn_4,btn_5,btn_6,
	btn_restar,btn_1,btn_2,btn_3,btn_sumar,btn_in,btn_negativo,btn_0,btn_coma,btn_igual;

	public static String addDig(String digito) {
		pantallita.setText(pantallita.getText() + digito);
		pantalla.setText(pantalla.getText() + digito);
		Historial.setText(Historial.getText() + digito);
		return digito;
	}
	
	public static int factorial(double num1) { //Sacado de StackOF
        int resultado = 1;
        for (int i = 1; i <= num1; i++) {
            resultado *= i;
        }
        return resultado;
    }
	
	public VentanaCalc() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 952, 571);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 0, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btn_BorrarHistorial = new JButton("X");
		btn_BorrarHistorial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Historial.setText(""); 	}
		});
		btn_BorrarHistorial.setForeground(Color.RED);
		btn_BorrarHistorial.setFont(new Font("Bahnschrift", Font.PLAIN, 22));
		btn_BorrarHistorial.setBackground(Color.BLACK);
		btn_BorrarHistorial.setBounds(870, 481, 52, 29);
		contentPane.add(btn_BorrarHistorial);
		
		//////Pantallas
		Historial = new JTextArea();
		Historial.setForeground(Color.WHITE);
		Historial.setFont(new Font("Bahnschrift", Font.PLAIN, 19));
		Historial.setEditable(false);
		Historial.setBackground(Color.BLACK);
		Historial.setBounds(611, 49, 311, 461);
		contentPane.add(Historial);
		
		JLabel txtHistorial = new JLabel("Historial");
		txtHistorial.setHorizontalAlignment(SwingConstants.LEFT);
		txtHistorial.setForeground(new Color(135, 206, 235));
		txtHistorial.setFont(new Font("Bahnschrift", Font.PLAIN, 17));
		txtHistorial.setBackground(Color.BLACK);
		txtHistorial.setBounds(611, 13, 65, 21);
		contentPane.add(txtHistorial);
	
		pantallita = new JTextField();
		pantallita.setBorder(javax.swing.BorderFactory.createEmptyBorder());
		pantallita.setBackground(new Color(0, 0, 0));
		pantallita.setHorizontalAlignment(SwingConstants.RIGHT);
		pantallita.setFont(new Font("Bahnschrift", Font.PLAIN, 17));
		pantallita.setEditable(false);
		pantallita.setBounds(12, 14, 568, 33);
		contentPane.add(pantallita);
		pantallita.setColumns(10);
		
		pantalla = new JTextField();
		pantalla.setForeground(Color.WHITE);
		pantalla.setBorder(javax.swing.BorderFactory.createEmptyBorder());
		pantalla.setBackground(new Color(0, 0, 0));
		pantalla.setHorizontalAlignment(SwingConstants.RIGHT);
		pantalla.setFont(new Font("Bahnschrift", Font.PLAIN, 59));
		pantalla.setEditable(false);
		pantalla.setColumns(10);
		pantalla.setBounds(12, 49, 568, 75);
		contentPane.add(pantalla);
		
		////////Numeros
		btn_1 = new JButton("1");
		btn_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addDig("1");
			}
		});
		btn_1.setBackground(new Color(0, 0, 0));
		btn_1.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_1.setForeground(new Color(255, 255, 255));
		btn_1.setBounds(126, 405, 111, 50);
		contentPane.add(btn_1);
		
		btn_2 = new JButton("2");			
		btn_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addDig("2");
			}
		});
		btn_2.setBackground(new Color(0, 0, 0));
		btn_2.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_2.setForeground(new Color(255, 255, 255));
		btn_2.setBounds(241, 405, 111, 50);
		contentPane.add(btn_2);
		
		btn_3 = new JButton("3");
		btn_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addDig("3");
			}
		});
		btn_3.setBackground(new Color(0, 0, 0));
		btn_3.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_3.setForeground(new Color(255, 255, 255));
		btn_3.setBounds(355, 405, 111, 50);
		contentPane.add(btn_3);
		
		btn_4 = new JButton("4");
		btn_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addDig("4");
			}
		});
		btn_4.setBackground(new Color(0, 0, 0));
		btn_4.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_4.setForeground(new Color(255, 255, 255));
		btn_4.setBounds(126, 350, 111, 50);
		contentPane.add(btn_4);
		
		btn_5 = new JButton("5");
		btn_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addDig("5");
			}
		});
		btn_5.setBackground(new Color(0, 0, 0));
		btn_5.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_5.setForeground(new Color(255, 255, 255));
		btn_5.setBounds(241, 350, 111, 50);
		contentPane.add(btn_5);
		
		btn_6 = new JButton("6");
		btn_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addDig("6");
			}
		});
		btn_6.setBackground(new Color(0, 0, 0));
		btn_6.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_6.setForeground(new Color(255, 255, 255));
		btn_6.setBounds(355, 350, 111, 50);
		contentPane.add(btn_6);
		
		btn = new JButton("7");
		btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addDig("7");
			}
		});
		btn.setBackground(new Color(0, 0, 0));
		btn.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn.setForeground(new Color(255, 255, 255));
		btn.setBounds(126, 295, 111, 50);
		contentPane.add(btn);
		
		btn_8 = new JButton("8");
		btn_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addDig("8");
			}
		});
		btn_8.setBackground(new Color(0, 0, 0));
		btn_8.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_8.setForeground(new Color(255, 255, 255));
		btn_8.setBounds(241, 295, 111, 50);
		contentPane.add(btn_8);
		
		btn_9 = new JButton("9");
		btn_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addDig("9");
			}
		});
		btn_9.setBackground(new Color(0, 0, 0));
		btn_9.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_9.setForeground(new Color(255, 255, 255));
		btn_9.setBounds(355, 295, 111, 50);
		contentPane.add(btn_9);
		
		btn_0 = new JButton("0");
		btn_0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addDig("0");
			}
		});
		btn_0.setBackground(new Color(0, 0, 0));
		btn_0.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_0.setForeground(new Color(255, 255, 255));
		btn_0.setBounds(241, 460, 111, 50);
		contentPane.add(btn_0);
		
		//////////////////////  Operadores		
		btn_pi = new JButton("π");
		btn_pi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			addDig("3.1416");	}
		});
		btn_pi.setBackground(new Color(112, 128, 144));
		btn_pi.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_pi.setForeground(new Color(255, 255, 255));
		btn_pi.setBounds(126, 133, 111, 50);
		contentPane.add(btn_pi);
	
		btn_e = new JButton("e");
		btn_e.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			addDig("2.7182818284590452353602874713527");}
		});
		btn_e.setBackground(new Color(112, 128, 144));
		btn_e.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_e.setForeground(new Color(255, 255, 255));
		btn_e.setBounds(241, 133, 111, 50);
		contentPane.add(btn_e);
		
		btn_c = new JButton("C");
		btn_c.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pantalla.setText("");
				pantallita.setText("");
				num1=0; 	num2=0;	}
		});
		btn_c.setBackground(new Color(165, 42, 42));
		btn_c.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_c.setForeground(new Color(255, 255, 255));
		btn_c.setBounds(355, 133, 111, 50);
		contentPane.add(btn_c);
		
		btn_Delete = new JButton("<x]");
		btn_Delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 if(pantalla.getText().length()!=0){
			            pantalla.setText(pantalla.getText().substring(0, pantalla.getText().length()-1));
				 }		 
				 if(pantallita.getText().length()!=0){
					 pantallita.setText(pantallita.getText().substring(0, pantallita.getText().length()-1));
				 }
				 if(Historial.getText().length()!=0){
					 Historial.setText(Historial.getText().substring(0, pantallita.getText().length()-1));}
			}
		});
		btn_Delete.setBackground(new Color(165, 42, 42));
		btn_Delete.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_Delete.setForeground(new Color(255, 255, 255));
		btn_Delete.setBounds(469, 133, 111, 50);
		contentPane.add(btn_Delete);
		
		btn_x2 = new JButton("x^2");
		btn_x2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num1= Double.parseDouble(pantalla.getText());
				op="x2"; 
				pantallita.setText("sqr(" + num1 + ")");
				pantalla.setText(Double.toString(num1 * num1));}
		});
		btn_x2.setBackground(new Color(112, 128, 144));
		btn_x2.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_x2.setForeground(new Color(255, 255, 255));
		btn_x2.setBounds(12, 187, 111, 50);
		contentPane.add(btn_x2);
		
		btn_1x = new JButton("1/x");
		btn_1x.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num1= Double.parseDouble(pantalla.getText());
				op="1x"; 
				pantalla.setText("");
				pantalla.setText(Double.toString(1/(num1)));}
		});
		btn_1x.setBackground(new Color(112, 128, 144));
		btn_1x.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_1x.setForeground(new Color(255, 255, 255));
		btn_1x.setBounds(126, 187, 111, 50);
		contentPane.add(btn_1x);

		btn_2vx = new JButton("2√x");
		btn_2vx.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num1= Double.parseDouble(pantalla.getText());
				addDig("√");
				op="v-"; 
				int Integral =(int) num1;
				pantalla.setText(Double.toString(Math.sqrt(Integral )));
				pantallita.setText("√(" + Integral + ")");
				}
		});
		btn_2vx.setBackground(new Color(112, 128, 144));
		btn_2vx.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_2vx.setForeground(new Color(255, 255, 255));
		btn_2vx.setBounds(12, 241, 111, 50);
		contentPane.add(btn_2vx);
		
		btn_parentesisAbrir = new JButton("(");
		btn_parentesisAbrir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addDig("(");
			}
		});
		btn_parentesisAbrir.setBackground(new Color(112, 128, 144));
		btn_parentesisAbrir.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_parentesisAbrir.setForeground(new Color(255, 255, 255));
		btn_parentesisAbrir.setBounds(126, 241, 111, 50);
		contentPane.add(btn_parentesisAbrir);
		
		btn_parentesisCerrar = new JButton(")");
		btn_parentesisCerrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addDig(")");}
		});
		btn_parentesisCerrar.setBackground(new Color(112, 128, 144));
		btn_parentesisCerrar.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_parentesisCerrar.setForeground(new Color(255, 255, 255));
		btn_parentesisCerrar.setBounds(241, 241, 111, 50);
		contentPane.add(btn_parentesisCerrar);
		
		btn_dividir = new JButton("÷");
		btn_dividir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num1= Double.parseDouble(pantalla.getText());
				op="÷";
				addDig("÷");
				pantalla.setText("");}
		});
		btn_dividir.setBackground(new Color(112, 128, 144));
		btn_dividir.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_dividir.setForeground(new Color(255, 255, 255));
		btn_dividir.setBounds(469, 241, 111, 50);
		contentPane.add(btn_dividir);
		
		btn_multiplicar = new JButton("X");
		btn_multiplicar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num1= Double.parseDouble(pantalla.getText());
				op="*"; 
				addDig("*");
				pantalla.setText(""); 	}
		});
		btn_multiplicar.setBackground(new Color(112, 128, 144));
		btn_multiplicar.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_multiplicar.setForeground(new Color(255, 255, 255));
		btn_multiplicar.setBounds(469, 295, 111, 50);
		contentPane.add(btn_multiplicar);
		
		btn_restar = new JButton("-");
		btn_restar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num1= Double.parseDouble(pantalla.getText());
				op="-";
				addDig("-");
				pantalla.setText("");}
		});
		btn_restar.setBackground(new Color(112, 128, 144));
		btn_restar.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_restar.setForeground(new Color(255, 255, 255));
		btn_restar.setBounds(469, 350, 111, 50);
		contentPane.add(btn_restar);
		
		btn_sumar = new JButton("+");
		btn_sumar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num1= Double.parseDouble(pantalla.getText());
				op="+";
				addDig("+");
				pantalla.setText("");

				}
		});
		btn_sumar.setBackground(new Color(112, 128, 144));
		btn_sumar.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_sumar.setForeground(new Color(255, 255, 255));
		btn_sumar.setBounds(469, 405, 111, 50);
		contentPane.add(btn_sumar);
		
		btn_negativo = new JButton("+/-");
		btn_negativo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			num1= Double.parseDouble(pantalla.getText());
			pantallita.setText("-" + num1 );
			pantalla.setText(Double.toString(num1 * -1));
			}
		});
		btn_negativo.setBackground(new Color(0, 0, 0));
		btn_negativo.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_negativo.setForeground(new Color(255, 255, 255));
		btn_negativo.setBounds(126, 460, 111, 50);
		contentPane.add(btn_negativo);
		
		btn_coma = new JButton(",");
		btn_coma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addDig(".");}
		});
		btn_coma.setBackground(new Color(0, 0, 0));
		btn_coma.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_coma.setForeground(new Color(255, 255, 255));
		btn_coma.setBounds(355, 460, 111, 50);
		contentPane.add(btn_coma);
		
		btn_exp = new JButton("exp");
		btn_exp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num1= Double.parseDouble(pantalla.getText());
				op="exp";
				addDig("e+");
				pantalla.setText("");
			}
		});
		btn_exp.setBackground(new Color(112, 128, 144));
		btn_exp.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_exp.setForeground(new Color(255, 255, 255));
		btn_exp.setBounds(355, 187, 111, 50);
		contentPane.add(btn_exp);
		
		btn_10x = new JButton("10^x");
		btn_10x.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num1= Double.parseDouble(pantalla.getText());
				op="10x";
				pantallita.setText("");
				Historial.setText("");
				addDig("10^"+num1);
				pantalla.setText("");
				pantalla.setText(Double.toString((Math.pow(10,num1))));}
		});
		btn_10x.setBackground(new Color(112, 128, 144));
		btn_10x.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_10x.setForeground(new Color(255, 255, 255));
		btn_10x.setBounds(12, 350, 111, 50);
		contentPane.add(btn_10x);
		
		btn_xy = new JButton("X^y");
		btn_xy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num1= Double.parseDouble(pantalla.getText());
				op="xy";
				addDig("^");
				pantalla.setText("");}
		});
		btn_xy.setBackground(new Color(112, 128, 144));
		btn_xy.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_xy.setForeground(new Color(255, 255, 255));
		btn_xy.setBounds(12, 295, 111, 50);
		contentPane.add(btn_xy);
		
		btn_log = new JButton("log");
		btn_log.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num1= Double.parseDouble(pantalla.getText());
				op="10x";
				pantallita.setText("");
				Historial.setText("");
				addDig("log("+num1+ ")");
				pantalla.setText("");
				pantalla.setText(Double.toString(Math.log(num1)));
			}
		});
		btn_log.setBackground(new Color(112, 128, 144));
		btn_log.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_log.setForeground(new Color(255, 255, 255));
		btn_log.setBounds(12, 405, 111, 50);
		contentPane.add(btn_log);
		
		btn_xBarras = new JButton("|x|");
		btn_xBarras.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num1= Double.parseDouble(pantalla.getText());
				pantallita.setText("abs("+num1+")");
				pantalla.setText(Double.toString(Math.abs(num1)));}
		});
		btn_xBarras.setBackground(new Color(112, 128, 144));
		btn_xBarras.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_xBarras.setForeground(new Color(255, 255, 255));
		btn_xBarras.setBounds(241, 187, 111, 50);
		contentPane.add(btn_xBarras);

		btn_mod = new JButton("mod");
		btn_mod.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num1= Double.parseDouble(pantalla.getText());
				op="mod";
				addDig("mod");
				pantalla.setText("");
			}
		});
		btn_mod.setBackground(new Color(112, 128, 144));
		btn_mod.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_mod.setForeground(new Color(255, 255, 255));
		btn_mod.setBounds(469, 187, 111, 50);
		contentPane.add(btn_mod);

		btn_n = new JButton("n!");
		btn_n.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num1= Double.parseDouble(pantalla.getText());
				op="n!";
				pantalla.setText(Double.toString(factorial(num1)));
			}
		});
		btn_n.setBackground(new Color(112, 128, 144));
		btn_n.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_n.setForeground(new Color(255, 255, 255));
		btn_n.setBounds(355, 241, 111, 50);
		contentPane.add(btn_n);
		
		btn_2nd = new JButton("2nd");
		btn_2nd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		
		});
		btn_2nd.setBackground(new Color(112, 128, 144));
		btn_2nd.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_2nd.setForeground(new Color(255, 255, 255));
		btn_2nd.setBounds(12, 133, 111, 50);
		contentPane.add(btn_2nd);
	
		btn_in = new JButton("in");
		btn_in.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btn_in.setBackground(new Color(112, 128, 144));
		btn_in.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_in.setForeground(new Color(255, 255, 255));
		btn_in.setBounds(12, 460, 111, 50);
		contentPane.add(btn_in);
		
		btn_igual = new JButton("=");
		btn_igual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num2=Double.parseDouble(pantalla.getText());
			
				switch (op) { 
				case "+":pantalla.setText(Double.toString(num1+num2)); 		
					break;
				case "*": pantalla.setText(Double.toString(num1*num2));
					break;
				case "-": pantalla.setText(Double.toString(num1-num2));
					break;
				case "/": pantalla.setText(Double.toString(num1/num2));
					break;
				case "%": pantalla.setText(Double.toString((num1*num2)/100)) ;
					break;
				case "exp": pantalla.setText(Double.toString(num1*(Math.pow(10,num2)))) ;
					break;
				case "xy": pantalla.setText(Double.toString(Math.pow(num1,num2)));
					break;
				case "mod": pantalla.setText(Double.toString(num1%num2));
					break;				
				}
				Historial.setText(Historial.getText() + " = " + pantalla.getText()+"\n");
				pantallita.setText(pantalla.getText());		
			}
		});
		btn_igual.setBackground(new Color(65, 105, 225));
		btn_igual.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_igual.setForeground(new Color(255, 255, 255));
		btn_igual.setBounds(469, 460, 111, 50);
		contentPane.add(btn_igual);
	}
}
	
