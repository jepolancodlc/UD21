package views;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import dto.calculos;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class VentanaConv extends JFrame {
	calculos c=new calculos();
	private JPanel contentPane;
	public static JTextField pantalla2,pantalla1;
	public static JComboBox<String> comboBox, comboBox2;
	private JButton btn_igual;
	double num,res;
	public static JTextField pantallasigno1, pantallasigno2;

	
	public static String addDig(String digito) {
		pantalla1.setText(pantalla1.getText() + digito);
		return digito;
	}
	
	public VentanaConv() {
		setResizable(false);
		setTitle("Conversor de Moneda");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 370, 621);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLACK);
		contentPane.setForeground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btn_0 = new JButton("0");
		btn_0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			addDig("0");
			}
		});
		
		pantallasigno2 = new JTextField();
		pantallasigno2.setHorizontalAlignment(SwingConstants.RIGHT);
		pantallasigno2.setBorder(javax.swing.BorderFactory.createEmptyBorder());
		pantallasigno2.setFont(new Font("Bahnschrift", Font.PLAIN, 33));
		pantallasigno2.setBackground(Color.BLACK);
		pantallasigno2.setForeground(Color.WHITE);
		pantallasigno2.setEditable(false);
		pantallasigno2.setBounds(186, 170, 45, 40);
		contentPane.add(pantallasigno2);
		pantallasigno2.setColumns(10);
		
		pantallasigno1 = new JTextField();
		pantallasigno1.setHorizontalAlignment(SwingConstants.RIGHT);
		pantallasigno1.setBorder(javax.swing.BorderFactory.createEmptyBorder());
		pantallasigno1.setBackground(Color.BLACK);
		pantallasigno1.setForeground(Color.WHITE);
		pantallasigno1.setFont(new Font("Bahnschrift", Font.PLAIN, 33));
		pantallasigno1.setEditable(false);
		pantallasigno1.setBounds(186, 40, 45, 40);
		contentPane.add(pantallasigno1);
		pantallasigno1.setColumns(10);
		btn_0.setForeground(Color.WHITE);
		btn_0.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_0.setBackground(Color.BLACK);
		btn_0.setBounds(127, 525, 111, 50);
		contentPane.add(btn_0);
		
		JButton btn_1 = new JButton("1");
		btn_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addDig("1");}
		});
		btn_1.setForeground(Color.WHITE);
		btn_1.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_1.setBackground(Color.BLACK);
		btn_1.setBounds(12, 470, 111, 50);
		contentPane.add(btn_1);
		
		JButton btn_2 = new JButton("2");
		btn_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addDig("2");}
		});
		btn_2.setForeground(Color.WHITE);
		btn_2.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_2.setBackground(Color.BLACK);
		btn_2.setBounds(127, 470, 111, 50);
		contentPane.add(btn_2);
		
		JButton btn_3 = new JButton("3");
		btn_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addDig("3");}
		});
		btn_3.setForeground(Color.WHITE);
		btn_3.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_3.setBackground(Color.BLACK);
		btn_3.setBounds(241, 470, 111, 50);
		contentPane.add(btn_3);
		
		JButton btn_4 = new JButton("4");
		btn_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addDig("4");}
		});
		btn_4.setForeground(Color.WHITE);
		btn_4.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_4.setBackground(Color.BLACK);
		btn_4.setBounds(12, 415, 111, 50);
		contentPane.add(btn_4);
		
		JButton btn_5 = new JButton("5");
		btn_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addDig("5");}
		});
		btn_5.setForeground(Color.WHITE);
		btn_5.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_5.setBackground(Color.BLACK);
		btn_5.setBounds(127, 415, 111, 50);
		contentPane.add(btn_5);
		
		JButton btn_6 = new JButton("6");
		btn_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addDig("6");
				}
		});
		btn_6.setForeground(Color.WHITE);
		btn_6.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_6.setBackground(Color.BLACK);
		btn_6.setBounds(241, 415, 111, 50);
		contentPane.add(btn_6);
		
		JButton btn = new JButton("7");
		btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addDig("7");}
		});
		btn.setForeground(Color.WHITE);
		btn.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn.setBackground(Color.BLACK);
		btn.setBounds(12, 360, 111, 50);
		contentPane.add(btn);
		
		JButton btn_8 = new JButton("8");
		btn_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addDig("8");	}
		});
		btn_8.setForeground(Color.WHITE);
		btn_8.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_8.setBackground(Color.BLACK);
		btn_8.setBounds(127, 360, 111, 50);
		contentPane.add(btn_8);
		
		JButton btn_9 = new JButton("9");
		btn_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addDig("9");}
		});
		btn_9.setForeground(Color.WHITE);
		btn_9.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_9.setBackground(Color.BLACK);
		btn_9.setBounds(241, 360, 111, 50);
		contentPane.add(btn_9);
		
		JButton btn_coma = new JButton(",");
		btn_coma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addDig(".");}
		});
		btn_coma.setForeground(Color.WHITE);
		btn_coma.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_coma.setBackground(Color.BLACK);
		btn_coma.setBounds(241, 525, 111, 50);
		contentPane.add(btn_coma);
		
		JButton btn_c = new JButton("C");
		btn_c.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pantalla1.setText("");
				pantalla2.setText("");}
		});
		btn_c.setForeground(Color.WHITE);
		btn_c.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_c.setBackground(new Color(165, 42, 42));
		btn_c.setBounds(127, 308, 111, 50);
		contentPane.add(btn_c);
		
		JButton btn_Delete = new JButton("<x]");
		btn_Delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(pantalla1.getText().length()!=0){
		            pantalla1.setText(pantalla1.getText().substring(0, pantalla1.getText().length()-1));
			 }}
		});
		btn_Delete.setForeground(Color.WHITE);
		btn_Delete.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_Delete.setBackground(new Color(165, 42, 42));
		btn_Delete.setBounds(241, 308, 111, 50);
		contentPane.add(btn_Delete);
		
		pantalla1 = new JTextField();
		pantalla1.setBackground(Color.BLACK);
		pantalla1.setForeground(Color.WHITE);
		pantalla1.setFont(new Font("Bahnschrift", Font.PLAIN, 33));
		pantalla1.setEditable(false);
		pantalla1.setBounds(12, 13, 228, 94);
		contentPane.add(pantalla1);
		pantalla1.setColumns(10);
	
		
		comboBox = new JComboBox<String>();
		comboBox.setFont(new Font("Bahnschrift", Font.PLAIN, 16));
		comboBox.setBackground(Color.BLACK);
		comboBox.setForeground(Color.WHITE);
		comboBox.setModel(new DefaultComboBoxModel<String>(new String[] {"Dolar", "Euro", "Yen", "Libra", "Peso Dominicano"}));
		comboBox.setSelectedIndex(0);
		comboBox.setBounds(12, 108, 228, 22);
		contentPane.add(comboBox);
		
		pantalla2 = new JTextField();
		pantalla2.setHorizontalAlignment(SwingConstants.LEFT);
		pantalla2.setBackground(Color.BLACK);
		pantalla2.setForeground(Color.WHITE);
		pantalla2.setFont(new Font("Bahnschrift", Font.PLAIN, 33));
		pantalla2.setEditable(false);
		pantalla2.setColumns(10);
		pantalla2.setBounds(12, 143, 225, 94);
		contentPane.add(pantalla2);
		
		comboBox2 = new JComboBox<String>();
		comboBox2.setBackground(Color.BLACK);
		comboBox2.setForeground(Color.WHITE);
		comboBox2.setFont(new Font("Bahnschrift", Font.PLAIN, 16));
		comboBox2.setModel(new DefaultComboBoxModel<String>(new String[] {"Dolar", "Euro", "Yen", "Libra", "Peso Dominicano"}));
		comboBox2.setSelectedIndex(1);
		comboBox2.setBounds(12, 238, 228, 22);
		contentPane.add(comboBox2);
		
		btn_igual = new JButton("=");
		btn_igual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				c.calcul();
			} 
		});
		btn_igual.setForeground(Color.WHITE);
		btn_igual.setFont(new Font("Bahnschrift", Font.PLAIN, 18));
		btn_igual.setBackground(new Color(65, 105, 225));
		btn_igual.setBounds(12, 525, 111, 50);
		contentPane.add(btn_igual);		
		
		JButton btnNewButton = new JButton("i");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			JOptionPane.showMessageDialog(null, "Datos actualizados por ultima vez el 30/08/2020 a las 23:00");}
		});
		btnNewButton.setBackground(Color.BLACK);
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setBounds(307, 13, 45, 25);
		contentPane.add(btnNewButton);
	}
}
