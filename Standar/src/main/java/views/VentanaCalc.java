package views;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class VentanaCalc extends JFrame {
	private static final long serialVersionUID = 3051160224425960686L;
	
	private double num1, num2;
	private String op;
	private static JTextField pantallita, pantalla;
	private static JTextArea Historial;
	private JButton btn_C,btnDelete,btn_1x,btn_x2,btn_raiz,btn_dividir,btn_7, btn_8, btn_9, btn_multiplicar, 
	btn_4, btn_5, btn_6,btn_Restar,btn_1, btn_2, btn_3, btn_sumar,btn_masmenos,btn_0,btn_coma,btn_igual,btn_CE;
	
	public static String addNum(String digito) {
		pantallita.setText(pantallita.getText() + digito);
		pantalla.setText(pantalla.getText() + digito);
		Historial.setText(Historial.getText() + digito);
		return digito;	
	}
	
	public VentanaCalc() {
		JPanel contentPane;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 673, 558);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLACK);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btn_BorrarHistorial = new JButton("X");
		btn_BorrarHistorial.setFont(new Font("Bahnschrift", Font.PLAIN, 22));
		btn_BorrarHistorial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Historial.setText(""); 	
			}
		});
		
		JLabel txtHistorial = new JLabel("Historial");
		txtHistorial.setHorizontalAlignment(SwingConstants.LEFT);
		txtHistorial.setBackground(new Color(0, 0, 0));
		txtHistorial.setFont(new Font("Bahnschrift", Font.PLAIN, 17));
		txtHistorial.setForeground(new Color(135, 206, 235));
		txtHistorial.setBounds(383, 24, 186, 20);
		contentPane.add(txtHistorial);
		
		btn_BorrarHistorial.setBackground(new Color(0, 0, 0));
		btn_BorrarHistorial.setForeground(new Color(255, 255, 255));
		btn_BorrarHistorial.setBounds(597, 476, 52, 29);
		contentPane.add(btn_BorrarHistorial);

		///Pantallas
		pantallita = new JTextField();
		pantallita.setBorder(javax.swing.BorderFactory.createEmptyBorder());
		pantallita.setFont(new Font("Bahnschrift", Font.PLAIN, 20));
		pantallita.setHorizontalAlignment(SwingConstants.RIGHT);
		pantallita.setBackground(Color.BLACK);
		pantallita.setEditable(false);
		pantallita.setBounds(12, 13, 320, 40);
		contentPane.add(pantallita);

		pantalla = new JTextField();
		pantalla.setBorder(javax.swing.BorderFactory.createEmptyBorder());
		pantalla.setHorizontalAlignment(SwingConstants.RIGHT);
		pantalla.setForeground(SystemColor.text);
		pantalla.setFont(new Font("Bahnschrift", Font.PLAIN, 50));
		pantalla.setEditable(false);
		pantalla.setBackground(Color.BLACK);
		pantalla.setBounds(12, 52, 320, 90);
		contentPane.add(pantalla);
		
		Historial = new JTextArea();
		Historial.setEditable(false);
		Historial.setBackground(Color.BLACK);
		Historial.setForeground(Color.WHITE);
		Historial.setFont(new Font("Arial", Font.PLAIN, 18));
		Historial.setBounds(383, 62, 266, 411);
		contentPane.add(Historial);
		
		//Botones con los numeros
		btn_1 = new JButton("1");
		btn_1.setFont(new Font("Bahnschrift", Font.PLAIN, 22));
		btn_1.setBackground(Color.BLACK);
		btn_1.setForeground(SystemColor.text);
		btn_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				addNum("1");
			}
		});
		btn_1.setBounds(12, 386, 80, 60);
		contentPane.add(btn_1);
		
		btn_2 = new JButton("2");
		btn_2.setFont(new Font("Bahnschrift", Font.PLAIN, 22));
		btn_2.setBackground(Color.BLACK);
		btn_2.setForeground(SystemColor.text);
		btn_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				addNum("2");
			}
		});
		btn_2.setBounds(92, 386, 80, 60);
		contentPane.add(btn_2);
		
		btn_3 = new JButton("3");
		btn_3.setFont(new Font("Bahnschrift", Font.PLAIN, 22));
		btn_3.setBackground(Color.BLACK);
		btn_3.setForeground(SystemColor.text);
		btn_3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				addNum("3");
			}
		});
	
		btn_3.setBounds(172, 386, 80, 60);
		contentPane.add(btn_3);	
		
		btn_4 = new JButton("4");
		btn_4.setFont(new Font("Bahnschrift", Font.PLAIN, 22));
		btn_4.setBackground(Color.BLACK);
		btn_4.setForeground(SystemColor.text);
		btn_4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				addNum("4");
			}
		});
		btn_4.setBounds(12, 327, 80, 60);
		contentPane.add(btn_4);
		
		btn_5 = new JButton("5");
		btn_5.setFont(new Font("Bahnschrift", Font.PLAIN, 22));
		btn_5.setBackground(Color.BLACK);
		btn_5.setForeground(SystemColor.text);
		btn_5.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				addNum("5");
			}
		});
		btn_5.setBounds(92, 327, 80, 60);
		contentPane.add(btn_5);
		
		btn_6 = new JButton("6");
		btn_6.setFont(new Font("Bahnschrift", Font.PLAIN, 22));
		btn_6.setBackground(Color.BLACK);
		btn_6.setForeground(SystemColor.text);
		btn_6.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				addNum("6");
			}
		});
		btn_6.setBounds(172, 327, 80, 60);
		
		contentPane.add(btn_6);
		btn_7 = new JButton("7");
		btn_7.setFont(new Font("Bahnschrift", Font.PLAIN, 22));
		btn_7.setBackground(Color.BLACK);
		btn_7.setForeground(SystemColor.text);
		btn_7.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				addNum("7");
			}
		});
		btn_7.setBounds(12, 268, 80, 60);
		contentPane.add(btn_7);
		
		btn_8 = new JButton("8");
		btn_8.setFont(new Font("Bahnschrift", Font.PLAIN, 22));
		btn_8.setBackground(Color.BLACK);
		btn_8.setForeground(SystemColor.text);
		btn_8.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				addNum("8");
			}
		});
		btn_8.setBounds(92, 268, 80, 60);
		contentPane.add(btn_8);
		
		btn_9 = new JButton("9");
		btn_9.setFont(new Font("Bahnschrift", Font.PLAIN, 22));
		btn_9.setBackground(Color.BLACK);
		btn_9.setForeground(SystemColor.text);
		btn_9.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				addNum("9");
			}
		});
		btn_9.setBounds(172, 268, 80, 60);
		contentPane.add(btn_9);
		
		btn_0 = new JButton("0");
		btn_0.setFont(new Font("Bahnschrift", Font.PLAIN, 22));
		btn_0.setBackground(Color.BLACK);
		btn_0.setForeground(SystemColor.text);
		btn_0.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				addNum("0");
			}
		});
		btn_0.setBounds(92, 445, 80, 60);
		contentPane.add(btn_0);
		
		//Operadores
		btn_sumar = new JButton("+");
		btn_sumar.setFont(new Font("Bahnschrift", Font.PLAIN, 22));
		btn_sumar.setBackground(new Color(0, 0, 51));
		btn_sumar.setForeground(SystemColor.text);
		btn_sumar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				num1= Double.parseDouble(pantalla.getText());
				op="+";
				addNum("+");
				pantalla.setText("");
			}
		});
		btn_sumar.setBounds(252, 386, 80, 60);
		contentPane.add(btn_sumar);

		btn_Restar = new JButton("-");
		btn_Restar.setFont(new Font("Bahnschrift", Font.PLAIN, 22));
		btn_Restar.setBackground(new Color(0, 0, 51));
		btn_Restar.setForeground(SystemColor.text);
		btn_Restar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				num1= Double.parseDouble(pantalla.getText());
				op="-";
				addNum("-");
				pantalla.setText("");
			}
		});
		btn_Restar.setBounds(252, 327, 80, 60);
		contentPane.add(btn_Restar);
		
	 	btn_dividir = new JButton("/");
		btn_dividir.setFont(new Font("Bahnschrift", Font.PLAIN, 22));
		btn_dividir.setBackground(new Color(0, 0, 51));
		btn_dividir.setForeground(SystemColor.text);
		btn_dividir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				num1= Double.parseDouble(pantalla.getText());
				op="/";
				addNum("/");
				pantalla.setText("");
			}
		});
		btn_dividir.setBounds(252, 208, 80, 60);
		contentPane.add(btn_dividir);
		
		
		
		btn_multiplicar = new JButton("X");
		btn_multiplicar.setFont(new Font("Bahnschrift", Font.PLAIN, 22));
		btn_multiplicar.setBackground(new Color(0, 0, 51));
		btn_multiplicar.setForeground(SystemColor.text);
		btn_multiplicar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				num1= Double.parseDouble(pantalla.getText());
				op="*"; 
				addNum("*");
				pantalla.setText(""); 
			}
		});
		btn_multiplicar.setBounds(252, 268, 80, 60);
		contentPane.add(btn_multiplicar);
		
		btnDelete = new JButton("<x]");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 if(pantalla.getText().length()!=0){
			            pantalla.setText(pantalla.getText().substring(0, pantalla.getText().length()-1));
				 }		 
				 if(pantallita.getText().length()!=0){
					 pantallita.setText(pantallita.getText().substring(0, pantallita.getText().length()-1));
				 }
				 
				 if(Historial.getText().length()!=0){
					 Historial.setText(Historial.getText().substring(0, pantallita.getText().length()-1));
				 }
			}
		});
		btnDelete.setFont(new Font("Bahnschrift", Font.PLAIN, 22));
		btnDelete.setBackground(new Color(0, 0, 51));
		btnDelete.setForeground(SystemColor.text);
		btnDelete.setBounds(252, 148, 80, 60);
		contentPane.add(btnDelete);
		
		btn_1x = new JButton("1/x");
		btn_1x.setFont(new Font("Bahnschrift", Font.PLAIN, 22));
		btn_1x.setBackground(new Color(0, 0, 51));
		btn_1x.setForeground(SystemColor.text);
		btn_1x.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				num1= Double.parseDouble(pantalla.getText());
				op="1x"; 
				pantalla.setText("");
				 pantalla.setText(Double.toString(1/(num1))); 
			}
		});
		btn_1x.setBounds(12, 208, 80, 60);
		contentPane.add(btn_1x);
		
		btn_x2 = new JButton("x^2");
		btn_x2.setFont(new Font("Bahnschrift", Font.PLAIN, 22));
		btn_x2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btn_x2.setBackground(new Color(0, 0, 51));
		btn_x2.setForeground(SystemColor.text);
		btn_x2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				num1= Double.parseDouble(pantalla.getText());
				op="e2"; 
				pantallita.setText("sqr(" + num1 + ")");
				pantalla.setText(Double.toString(num1 * num1));
			}
		});
		btn_x2.setBounds(92, 208, 80, 60);
		contentPane.add(btn_x2);
		
		btn_raiz = new JButton("2√x");
		
		btn_raiz.setFont(new Font("Arial", Font.PLAIN, 22));
		btn_raiz.setBackground(new Color(0, 0, 51));
		btn_raiz.setForeground(SystemColor.text);
		btn_raiz.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				num1= Double.parseDouble(pantalla.getText());
				int Integral= (int) num1;
				addNum("√");
				op="v-"; 
				pantalla.setText(Double.toString(Math.sqrt(Integral)));
				pantallita.setText("√(" + Integral + ")");
			}
		});
		btn_raiz.setBounds(172, 208, 80, 60);
		contentPane.add(btn_raiz);
		
		JButton btn_porcentaje = new JButton("%");
		btn_porcentaje.setFont(new Font("Bahnschrift", Font.PLAIN, 22));
		btn_porcentaje.setBackground(new Color(0, 0, 51));
		btn_porcentaje.setForeground(SystemColor.text);
		btn_porcentaje.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				num1= Double.parseDouble(pantalla.getText());
				op="%"; 
				addNum("");
				pantalla.setText("");
			}
		});
		btn_porcentaje.setBounds(12, 148, 80, 60);
		contentPane.add(btn_porcentaje);

		btn_masmenos = new JButton("+/-");
		btn_masmenos.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				num1= Double.parseDouble(pantalla.getText());
				pantallita.setText("-" + num1 );
				pantalla.setText(Double.toString(num1 * -1));}
		});
		btn_masmenos.setFont(new Font("Bahnschrift", Font.PLAIN, 22));
		btn_masmenos.setBackground(Color.BLACK);
		btn_masmenos.setForeground(SystemColor.text);
		btn_masmenos.setBounds(12, 445, 80, 60);
		contentPane.add(btn_masmenos);
		
		btn_coma = new JButton(",");
		btn_coma.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
			addNum(".");	}
		});
		btn_coma.setFont(new Font("Bahnschrift", Font.PLAIN, 22));
		btn_coma.setBackground(Color.BLACK);
		btn_coma.setForeground(SystemColor.text);
		btn_coma.setBounds(172, 445, 80, 60);
		contentPane.add(btn_coma);
		
		
		btn_CE = new JButton("CE");
		btn_CE.setFont(new Font("Bahnschrift", Font.PLAIN, 22));
		btn_CE.setBackground(new Color(0, 0, 51));
		btn_CE.setForeground(SystemColor.text);
		btn_CE.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				pantalla.setText("");
			}
		});
		btn_CE.setBounds(92, 148, 80, 60);
		contentPane.add(btn_CE);
		
		btn_C = new JButton("C");
		btn_C.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		
		btn_C.setFont(new Font("Bahnschrift", Font.PLAIN, 22));
		btn_C.setBackground(new Color(0, 0, 51));
		btn_C.setForeground(SystemColor.text);
		btn_C.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				pantalla.setText("");
				pantallita.setText("");
				num1=0; 	num2=0;
			}
		});
		btn_C.setBounds(172, 148, 80, 60);
		contentPane.add(btn_C);
		
		btn_igual = new JButton("=");
		btn_igual.setFont(new Font("Bahnschrift", Font.PLAIN, 22));
		btn_igual.setForeground(SystemColor.text);
		btn_igual.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			num2=Double.parseDouble(pantalla.getText());

			switch (op) { 
			case "+":pantalla.setText(Double.toString(num1+num2)); 		
				break;
			case "*": pantalla.setText(Double.toString(num1*num2));
				break;
			case "-": pantalla.setText(Double.toString(num1-num2));
				break;
			case "/": pantalla.setText(Double.toString(num1/num2));
				break;
			case "%": pantalla.setText(Double.toString((num1*num2)/100)) ;
				break;
				}
			
			Historial.setText(Historial.getText() + " = " + pantalla.getText()+"\n");
			pantallita.setText(pantalla.getText());	
			}
		});
		btn_igual.setBackground(SystemColor.textHighlight);
		btn_igual.setBounds(252, 445, 80, 60);
		contentPane.add(btn_igual);
		
		pantallita.setText(pantalla.getText());
	}
}